# TelegramEMEBot
# Ville Jussila (wilho / OH8ETB)
# https://jkry.org/ouluhack/TelegramEMEBot
# 
# Free as a free beer or something.. 
# use and do what ever you want, i will be pleased if you mention me on credits.
#
# In general you should not be any reason to run code as there is bot running. Just start talking to "wsjt_bot" in telegram
#
#

#General TODO:
#
# *Create funcfion for sending messages to client, will get paremeters does it need to notify and if it will throw "user blocked" execption will shutdown instance
# *thread and TelegramEMEBot instance handling (mayby add instance to global users dict?)
# *bug where bot will ask Email after using certain commands??
#

import sys
import time
import telepot
import telepot.helper
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton,ForceReply
from telepot.delegate import (
    per_chat_id, create_open, pave_event_space, include_callback_query_chat_id)
from bs4 import BeautifulSoup
import os
import datetime
import pycurl
import re
import string
try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO
import logging
import threading
import Queue
import os.path
import datetime
import glob
import getopt


#setup logging
logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()

fileHandler = logging.FileHandler("bot.log")
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)

rootLogger.setLevel(logging.INFO)

#get important parameters

TOKEN = False
adminid = False

try:
    options, remainder = getopt.getopt(sys.argv[1:], 't:a:h')
except getopt.GetoptError as e:
    print("Usage: TelegramEMEBot.py -t TOKEN -a admin_telegram_id" )
    sys.exit(2)

for opt, arg in options:
    if opt == '-h':
        print("Usage: TelegramEMEBot.py -t TOKEN -a admin_telegram_id")
        sys.exit(1)
    elif opt == '-t':
        TOKEN = arg
    elif opt == '-a':
        adminid = arg
if TOKEN == False:
    print 'TOKEN required'
    print("Usage: TelegramEMEBot.py -t TOKEN -a admin_telegram_id")
    sys.exit(1)
if adminid == False:
    print 'not received admin telegram id. Status updates wont be send for admin'



#this is thread save global dict for online user, key is telegram userid (self.id)
users = telepot.helper.SafeDict() 

# class which will be created when someone says something to bot, will include actual message loop, sending messages to chat, registering bot to wsjt chat
# TODO change name to TelegramEMEBot
# TODO move stripping function to class and rename it to parseLine or something similar
class TelegramEMEBot(telepot.helper.ChatHandler):

    #Forcereply object for getting those info from user
    forcereply = ForceReply(force_reply = True)
    
    def __init__(self, *args, **kwargs):
        super(TelegramEMEBot, self).__init__(*args, **kwargs)
        logging.info( "running init for:"+str(self.id))
        self.idents = []
        self.tempusers = str(users)
        # Retrieve from database
        global users
        self.latestmsg =  datetime.datetime.strptime("201622Oct10:29", '%Y%d%b%H:%M') #TODO: fix something more elegant like now()-2days
        self.reg = False
        self.ready = False
        self.livecq = ""
        self.messages = []
        self.call = "NotSet"
        self.name = "NotSet"
        self.locator = "NotSet"
        self.state = "NotSet"
        self.email = "NotSet"
        self.stop_loop = False
        self.loop_running = False
        self.read_info_from_cookie() #TODO add exception handling
        self.check_idents()
        self.qrz_log_key = None
        self.blocked = False # if user has blocked bot
        self.calls = []
        self.show_worked = False # do we check every call if on log (and add ! mark before call)
        users[self.id] = (self.id, datetime.datetime.now(), datetime.datetime.now(), self.call, self.name, self.locator,"online")
        self.read_user_settings()
    # read idents.txt for msgids
    # TODO: think something more elegant than getting tru all messages send
    def check_idents(self,msg=None):
        if msg == None:
            try:
                if os.path.isfile("tg_users"):
                    logging.info("file is there")

                    for line in open("tg_users"):
                        self.idents.append(line.rstrip())
                else:
                    logging.info( "no idents file found " +str(self.id))
                    self.idents = []
            except Exception as e:
                logging.error( "something went wrong on reading idents.txt: "+str(e))
        else:
            new = 1
            
            for i in self.idents:
                if str(msg['from']) == i:
                    new = 0
            if new == 1:
                self.idents.append(str(msg['from']))
                try:
                    logging.info("writing ident to file")
                    with open("tg_users", "a") as myfile:
                        myfile.write(str(msg['from'])+'\n')    
                except Exception as e:
                    logging.error("something went wrong on writing idents.txt: " +str(e))     

    # start messaging loop and add it to separate thread
    def start_loop(self):
        self.stop_loop = False
        chatt = threading.Thread(target=self.chat_message_loop)
        chatt.daemon = True
        chatt.start()
    def write_user_settings(self):
        try:
            with open(str(self.id)+".settings", "w") as myfile:
                myfile.write("qrz_log "+self.qrz_log_key +'\n')
                myfile.write("show_worked "+str(self.show_worked) +'\n')
        except Exception as e:
            logging.error( "something went wrong on writing settings: " + str(self.id) +": "+str(e))

    def read_user_settings(self):
        try:
            if os.path.isfile(str(self.id)+".settings"):
                logging.info("found settings!")

                for line in open(str(self.id)+".settings"):
                    if "qrz_log" in line:
                        self.qrz_log_key = line.split()[1]
                    elif "show_worked" in line:
                        self.show_worked = line.split()[1] in ['True']


            else:
                logging.info( "no settings found for user " +str(self.id))
        except Exception as e:
            logging.error( "something went wrong on reading settings: " + str(self.id) +": "+str(e))


    #get user details from cookies created / saved by wsjt registeration
    def read_info_from_cookie(self):
        try:
            if os.path.isfile(str(self.id)+".txt"):
                logging.info("found cookie!")
            
                for line in open(str(self.id)+".txt"):
                    if "www.chris.org" in line:
                        if line.split()[5] == "callsign":
                            self.call = line.split()[6]
                        elif line.split()[5] == "name":
                            self.name = line.split()[6]
                        elif line.split()[5] == "locator":
                            self.locator = line.split()[6]
                        elif line.split()[5] == "state":
                            self.state = line.split()[6]
                        elif line.split()[5] == "email":
                            self.email = line.split()[6]
            else:
                logging.info( "no cookie found for user " +str(self.id))
        except Exception as e:
            logging.error( "something went wrong on reading cookie: " + str(self.id) +": "+str(e))

    #Stop messaging loop
    def stopmessageloop(self):
        self.stop_loop = True
        while self.loop_running:
            logging.info( "waiting for loop to shutdown")
            time.sleep(2)


    #What to do when message arrives from user    
    def on_chat_message(self, msg):
        #try:
        logging.info( msg)
        self.check_idents(msg)
 
        #Handles all the commands
        if msg['text'][0] == "/":
            logging.info( "received command msg: "+ msg['text'])
            
            #Restart Command
            if msg['text'] == "/restart":
                self.stop_loop = True
                self.sender.sendMessage('Restarting messaging loop, please standby.')
                while self.loop_running:
                    logging.info( "waiting for loop to shutdown")
                    time.sleep(2)
                self.stop_loop = False
                self.start_loop()

            #Every new user which start messaging with bot will send /start 
            elif msg['text'] == "/start":
                self.sender.sendMessage('Hello! wait a while and I will start showing you messages from wsjt chat.. More info about me see https://jkry.org/ouluhack/TelegramEMEBot')
                if self.call == "NotSet":
                    self.sender.sendMessage('I wont let you say something to wsjt chat before you have given me your user details what to use in chat.. if you just wanna lurk just ignore my questions')
                    time.sleep(10)
                    self._ask_reply('Give your call sign.')
                time.sleep(5)
                self.start_loop()             
             #Change userdetails
            elif msg['text'] == "/change":
                self.sender.sendMessage('User info has been reseted.')
                self.call = "NotSet"
                self.name =  "NotSet"
                self.locator = "NotSet"
                self.state =  "NotSet"
                self.email =  "NotSet"
                if os.path.isfile(str(self.id)+".txt"):
                    os.remove(str(self.id)+".txt")
                self.reg = False
                self.ready = False
                self._ask_reply('Give your call sign.')
                users[self.id] = (self.id,users[self.id][1], users[self.id][2], self.call, self.name, self.locator,"online")
        #except Exception as e:
            #logging.error("failed to get message "+str(self.id)+" "+str(e))
         #self.sender.sendMessage('EH, what did you send to me? I can understand only text (and maybe some cw :) )')

            #stop whole instance    
            elif msg['text'] == "/stop":
                self.sender.sendMessage('Bye! You could aways restart messaging just saying something for me or by sending /start command.')
                #self.stop_loop = True
                #self.sender.sendMessage('Restarting messaging loop, please standby.')
                #while self.loop_running:
                #    logging.info( "waiting for loop to shutdown")
                #    time.sleep(2)
                self.reg = False
                self.ready = False
                #self.stop_loop = False
                self.close()
                #show userdetails TODO: parse users file for human readeable format
            elif msg['text'] == "/users":
                global users
                self.sender.sendMessage('List of users: '+parse_users(users))


            elif "/i" in msg['text']:
                    message = " ".join(msg['text'].split()[1:])
                    if len(message) > 0:
                        logging.info( "user " +str(self.id)+" "+self.call+" has asked info:: "+message)
                        self.sender.sendMessage(self.get_info(message.upper()))
                    else:
                        self.sender.sendMessage('That command works like \"/i call \" ')
            
                #send message to all other users.. 
            elif "/to_other_users" in msg['text']:
                global users
                for user in users:
                    logging.info(str(user))
                    message = " ".join(msg['text'].split()[1:])
                    print message
                    print len(message)
                    if len(message) > 0:
                        logging.info( "user " +str(self.id)+" "+self.call+" is sending message to others. message: "+message)
                        try:
                            self.bot.sendMessage(user, "*"+self.call+": "+message+"*",disable_notification = False,parse_mode = "Markdown")
                        except Exception as e:
                            logging.error("could not send message to: " + str(user) +": "+str(e))
                    else:
                        self.sender.sendMessage('That command works like \"/to_other_users here goes actual message \" and it will be send to all users both online and offline')



                #sets qrz.com logbook api key 
            elif "/set_qrz_key" in msg['text']:
                    message = " ".join(msg['text'].split()[1:])
                    if len(message) > 0:
                        logging.info( "user " +str(self.id)+" "+self.call+" set qrz logbook api key: "+message)
                        self.qrz_log_key = message
                        self.show_worked = True
                        self.write_user_settings()
                        self.sender.sendMessage("Thank you, Nextime I will show have you been worked with somebody with \'/i call\' command")
                    else:
                        self.sender.sendMessage('That command works like \"/set_qrz_key 112233445 \" ')
                        self.show_worked = False
                        self.write_user_settings()
            else:
                 self.sender.sendMessage('sorry, did not get it. (check your command).')


            #other than Commands
        else:
            try: 
                # if message is "reply_to_message" do things (used for getting user info)
                if 'reply_to_message' in msg:
                    if msg['reply_to_message']['text'] == 'Give your call sign.':
                        logging.info(  "found call" + msg['text'])
                        if self.validate_userinput(msg['text'],1):
                            self.call = msg['text']
                        else:
                            self.sender.sendMessage('There was something fishy (too long, wrong characters etc) about your input, try again')
                    elif msg['reply_to_message']['text'] == 'Give your Name.':
                        logging.info( "found name "+msg['text'])
                        if self.validate_userinput(msg['text'],2):
                            self.name = msg['text']
                        else:
                            self.sender.sendMessage('There was something fishy (too long, wrong characters etc) about your input, try again')
                    elif msg['reply_to_message']['text'] == 'Your locator?':
                        logging.info( "found locator "+msg['text'])
                        if self.validate_userinput(msg['text'],3):
                            self.locator = msg['text']
                        else:
                            self.sender.sendMessage('There was something fishy (too long, wrong characters etc) about your input, try again')
                    elif msg['reply_to_message']['text'] == 'Your state in 2 digit? if not from US use XX':
                        logging.info( "found state "+msg['text'])
                        if self.validate_userinput(msg['text'],4):
                            self.state = msg['text']
                        else:
                            self.sender.sendMessage('There was something fishy (too long, wrong characters etc) about your input, try again')
                    elif msg['reply_to_message']['text'] == 'Your email?':
                        logging.info( "found email "+msg['text'])
                        if self.validate_userinput(msg['text'],5):
                            self.email = msg['text']
                            self.sender.sendMessage('Thank you! Now we are all done, you are free to chat on jt65emeA chat via telegram! If you ever need to change these user info use /change command')
                            self.sender.sendMessage('please note that: YOUR NEXT MESSAGE WILL BE SEND DIRECTLY TO CHAT (ofc not those commands which starts with \"/\"')
                            global users
                            users[self.id] = (self.id, users[self.id][1], users[self.id][2], self.call, self.name, self.locator,"online")
                            self.ready = True
                            if self.reg == False:
                                self.register_to_site()
                        else:
                            self.sender.sendMessage('There was something fishy (too long, wrong characters etc) about your input, try again')

                logging.info( self.call + " " + self.name + " " + self.locator + " " + self.state + " " + self.email)

                # if / elseifs to get userdetails from user
                if self.call == "NotSet":
                    self._ask_reply('Give your call sign.')
                elif self.name ==  "NotSet":
                    self._ask_reply('Give your Name.')
                elif self.locator == "NotSet":
                    self._ask_reply('Your locator?')
                elif self.state ==  "NotSet":
                    self._ask_reply('Your state in 2 digit? if not from US use XX')
                elif self.email ==  "NotSet":
                    self._ask_reply('Your email?')
        
                # this structure prevents sending first message before even messaging loop is running
                else:
                    if self.ready and self.loop_running:
                        #print "FUCK! your almost send: "+msg['txt']+" to chat"  #this is for testing as you figure it out
                        self.send_message_to_chat(msg['text'])
                        #update users dictionary latest send message time
                        users[self.id] = (self.id, datetime.datetime.now(), users[self.id][2], self.call, self.name, self.locator,"online")
                    else:
                        if self.ready != True:
                            self.sender.sendMessage('Hello '+self.name+'! Your are now ready to send text to jt65emeA chat YOUR NEXT MESSAGE WILL BE SEND DIRECTLY TO CHAT (ofc not those commands which starts with \"/\"')
                            self.ready = True
                        if self.loop_running != True:
                            self.sender.sendMessage('Wait a while and I will start showing you messages from wsjt so that you know what you should talk about')
                            time.sleep(5)
                            self.start_loop()
            except Exception as e:
                logging.error("failed to get message "+str(self.id)+" "+str(e))
             #self.sender.sendMessage('EH, what did you send to me? I can understand only text (and maybe some cw :) )')

    #if someone send "edit message"
    def on_edited_chat_message(self, msg):
        self.sender.sendMessage('HI, what do you think would it be possible to edit messages on ageold http chat? ')

    #do some basic validation for user given data 
    def validate_userinput(self,msg,type=1):
        if type == 1: #this if for call eg. OH8ETB/2x9H/600
            allowed = set(string.ascii_lowercase +string.ascii_uppercase + string.digits + '/')
            maxlen = 25
            minlen = 3
        elif type == 2: #this if for name eg. Ville
            allowed = set(string.ascii_lowercase + string.ascii_uppercase)
            maxlen = 20
            minlen = 1
        elif type == 3: #this if for locator eg. KP35eg        
            allowed = set(string.ascii_lowercase + string.ascii_uppercase + string.digits )
            maxlen = 6
            minlen = 4
        elif type == 4: #this if for stae eg. xx         
            allowed = set(string.ascii_lowercase + string.ascii_uppercase)
            maxlen = 2
            minlen = 2
        elif type == 5: #this if for email eg. ville.jussila@gmail.com        
            allowed = set(string.ascii_lowercase + string.digits + '@'+ '-'+'_'+'.')
            maxlen = 40
            minlen = 4
        if maxlen >= len(msg) and minlen <= len(msg) and set(msg) <= allowed:
            logging.info("validate passed: "+msg+" type="+str(type)+" "+str(maxlen)+" "+str(minlen))
            return True
        else:
            logging.info("validate failed: "+msg+" type="+str(type)+" "+str(maxlen)+" "+str(minlen))
            return False

    #Function which do registeration to wsjt chat and stores cookie from there
    #TODO: why this throws everything to stdout?
    def register_to_site(self):
        try:
            c = pycurl.Curl()
            c.setopt(c.URL, 'http://www.chris.org/cgi-bin/jt65reg')
            data = 'callsign='+self.call+"&name="+self.name+"&locator="+self.locator+"&state="+self.state+"&email="+self.email
            logging.info( data)	
            c.setopt(c.POSTFIELDS, data)
            #c.setopt(c.VERBOSE, True)
            c.setopt(pycurl.COOKIEJAR, str(self.id)+".txt")
            c.setopt(pycurl.COOKIEFILE, str(self.id)+".txt")
            c.perform()
            self.reg = True
        except Exception as e:
            logging.error("something went wrong on registering site"+ str(e))
            self.sender.sendMessage('Failed to register your info to chat. Please check you userinfo and use /change if nessesary')

    def _ask_reply(self,text):
        sent = self.sender.sendMessage(text, reply_markup=self.forcereply)
        logging.info( sent)
        self._editor = telepot.helper.Editor(self.bot, sent)
        self._edit_msg_ident = telepot.message_identifier(sent)
        return sent['text']

    #Send message to wsjt chat 
    #TODO:prevent printing to stdout
    def send_message_to_chat(self,message):
        try:
            if self.reg == False:
                self.register_to_site()
            c = pycurl.Curl()
            c.setopt(c.URL, 'http://www.chris.org/cgi-bin/jt65emeA')
            data = "text="+message
            #print data
            c.setopt(c.POSTFIELDS, data)
            #c.setopt(c.VERBOSE, True)
            c.setopt(pycurl.COOKIEJAR, str(self.id)+".txt")
            c.setopt(pycurl.COOKIEFILE, str(self.id)+".txt")
            c.perform()
        except:
             logging.error("failed to send message to jt65emeA"+str(self.id))
    def qrz_logs(self,key,call):
        buffer = BytesIO()
        try:
            c = pycurl.Curl()
            c.setopt(c.URL, 'http://logbook.qrz.com/api')
            c.setopt(c.WRITEFUNCTION, buffer.write)
            data = 'KEY='+key+'&ACTION=FETCH&OPTION=BAND:2m,MODE:JT65,CALL:'+call+',TYPE:LOGIDS'
            print data
            c.setopt(c.POSTFIELDS, data)
            c.perform()
            results = buffer.getvalue()
            #self.reg = True
            print results

            for i in results.split('&'):
                if i.split('=')[0] == 'COUNT':
                    return int(i.split('=')[1])
            return None #results.split('&')[0].split('=')[1]
        except Exception as e:
                    #except:
            print "something went wrong on getting log data from qrz"+ str(e)
            return None
        #self.sender.sendMessage('Failed to register your info to chat. Please check you userinfo and use /change if nessesary')

    # function wich will be automaticly called when timeout will be reached
    def on__idle(self, event):
        logging.info("idle timeout"+str(self.id))
        self.stopmessageloop()
        self.sender.sendMessage('Dear '+self.name+' timeout has been reached. You could always restart chat messaging just saying something for me or by sending /start command')
        #self.sender.sendMessage('I know you may need a little time. I will always be here for you.')
        self.close()

    # what to do when intance is shutdown
    def on_close(self, ex):
        self.stopmessageloop()
        # Save to database
        global users
        self.sender.sendMessage('bye')
        users[self.id] = (self.id,users[self.id][1], users[self.id][2], self.call, self.name, self.locator,"offline")

    #function to get worked status (and worked dxcc) from different sources and add this to table for later usege (performance)
    def get_worked(self, call):
        worked = {'worked': 99,'call': 'none','dxcc': True}
        for c in self.calls:
            if c['call'] == call:
                print "found on loop"+str(c)
                
                worked = c
        if worked['call'] == 'none':
            worked= {'worked': self.qrz_logs(self.qrz_log_key,call), 'call':call,'dxcc':True}
            #worked['call'] = call
            #worked['dxcc'] = True   #TODO do actual function, to get callers DXCC and get logs for that from qrz.com
            self.calls.append(worked)
        
        return worked

    #prints detailed info about call (locator, full call, has it been worked from logs etc)
    def get_info(self,call):
        from_chat = self.get_chat_user_info(call)
        if from_chat == False:
            return "failed to get info"

        if self.qrz_log_key != None:
            times_worked = self.get_worked(call)['worked']#self.qrz_logs(self.qrz_log_key,call)
            print times_worked
            if times_worked > 0:
                return "ON LOG " + str(times_worked) +" times " + from_chat['fullcall'] + " "+from_chat['name']+" "+from_chat['loc']
            elif times_worked == 0:
                return "NOT ON LOG " + from_chat['fullcall'] + " "+from_chat['name']+" "+from_chat['loc']
            else:
                return "LOG FAIL" + from_chat['fullcall'] + " "+from_chat['name']+" "+from_chat['loc']

        else:

            return from_chat['fullcall'] + " "+from_chat['name']+" "+from_chat['loc']
        
        


    # Get user details from chat (locators, name, full call etc)
    def get_chat_user_info(self,call):
        try:
            for message in self.messages:
                if strip_message(message,3,self.call,self.name)['call'] == call:
                    return strip_message(message,3,self.call,self.name)
            return False

        except Exception as e:
            logging.error("something went wrong on getting chat user info"+str(self.id)+ str(e))
            return False   
    # Function to send message to user and catch "blocked by user exception"
    def send_to_user(self,message, mode=1,disable_notify=True):
        try:
            if mode == 1: #no markup
                self.sender.sendMessage(str(message),disable_notification = disable_notify)
            elif mode == 2: #markup
                self.sender.sendMessage(str(message),disable_notification = disable_notify,parse_mode = "Markdown")
            elif mode == 3: #all bold and notify
                self.sender.sendMessage("*"+message+"*",disable_notification = False,parse_mode = "Markdown")

        except Exception as e:
            logging.error("something went wrong on sending message to user"+str(self.id)+ str(e))

    # Main messaging loop, send new messages from wsjt chat to user as telegram message
    def chat_message_loop(self):
   
        # check if there is another loop instance running
        if self.loop_running:
            logging.info("another loop running, exiting")
            return
        self.loop_running = True

        #send messages to user, this will be compared to list from site
        self.messages = []

        global users

        # send backlog to user
        for line in reversed(sitedata.splitlines()):
            if '====== ' in line:    
                self.messages.append(line)

        for message in self.messages[-15:]:
            logging.info( strip_message(message,2,self.call,self.name)['message'])
            if strip_message(message,1,self.call,self.name)['notify']:
                self.sender.sendMessage("*"+strip_message(message)['message']+"*",disable_notification = False,parse_mode = "Markdown")
            else:
                self.sender.sendMessage(strip_message(message,2,self.call,self.name)['message'],disable_notification = True)
 
        # actual loop
        while True:
            #print len(messages)
            #print len(sitedata.splitlines())

            # shuts down messageloop if self.stop_loop True
            if self.stop_loop:
                logging.info( "stopping loop")
                self.loop_running = False
                return 

            #reads new data from site and compares it messages send to use
            # if new lines fond send those to user
            for line in reversed(sitedata.splitlines()):
                if '====== ' in line:
                    new = 1
                    for old in self.messages:
                        if old == line:
                            new = 0
                    if new == 1:
                        try:
                            stripped = strip_message(line,3,self.call,self.name)
                            #print line   
                            logging.info( str(self.call) + str(stripped))
                            print self.qrz_log_key
                            print self.show_worked
                            if self.show_worked:
                                print self.get_worked(stripped['call'])
                                if self.get_worked(stripped['call'])['dxcc'] == False : # if have not worked DXCC
                                    msg = "!!"+stripped['call'] + ":"+stripped['name']+':: ' + stripped['message']
                                elif self.get_worked(stripped['call'])['worked'] < 1: # if possible new INIT                     
                                    msg = "!"+stripped['call'] + ":"+stripped['name']+':: ' + stripped['message']
                                else:
                                    msg = stripped['call'] + ":"+stripped['name']+':: ' + stripped['message']
                            else:
                                msg = stripped['call'] + ":"+stripped['name']+':: ' + stripped['message']

                            # compare actual time of latest message send to user and "new" message received from compare.. 
                            # this is additional check for reducing risk of wrong messages send to user 
                            if stripped['time'] >= self.latestmsg:
                                self.latestmsg = stripped['time']

                                #TODO think where this should be, based on different not normal scenarios
                                self.messages.append(line)
                                self.messages.pop(0)
                                
                                # if message includes call,name or other "higlighted" matter, send message in bold, and enable notification on telegram
                                if stripped['notify']:
                                    self.send_to_user(msg,3)
                                else:
                                    self.send_to_user(msg)
                                    #self.sender.sendMessage(stripped['message'],disable_notification = True)
                            else:
                                logging.info("message older than latest")  
                        except Exception as e:
                        #except:
                            logging.error("something went wrong on sending message to TG:"+str(self.id)+ str(e))
            
            #TODO global again
            global livecq

            #prints livecq spots if changed (getting actual data has been disabled for now)
            if self.livecq != livecq:
                self.sender.sendMessage('_'+"LiveCQ: "+livecq+'_',disable_notification = True,parse_mode = "Markdown")
                self.livecq = livecq
                logging.info(livecq)

          
            time.sleep(3)

#TODO is this needed??
headers = {}
#get http from server 
def get_url(url):

    buffer = BytesIO()
    try:
        c = pycurl.Curl()
        c.setopt(c.URL, url)
        #c.setopt(pycurl.USERPWD, "%s:%s" % (u, p))
        c.setopt(c.WRITEFUNCTION, buffer.write)
        c.setopt(c.HEADERFUNCTION, header_function)
        c.perform()
         # HTTP response code, e.g. 200.
        logging.debug('Status: %d' % c.getinfo(c.RESPONSE_CODE))
        # Elapsed time for the transfer.
        logging.debug('Status: %f' % c.getinfo(c.TOTAL_TIME))
        c.close()
        body = buffer.getvalue()
        # Body is a string in some encoding.
        # In Python 2, we can print it without knowing what the encoding is.
        logging.debug((headers))
        logging.debug((body))
        return body
    except Exception as e:
        logging.error(self.name +" Failed to get url - "+ str(e))
        return buffer.getvalue()

#header function for get_url, strictly not needet atm, but maybe in future
def header_function(header_line):
    header_line = header_line.decode('iso-8859-1')
    if ':' not in header_line:
        return
    name, value = header_line.split(':', 1)
    name = name.strip()
    value = value.strip()

    name = name.lower()
    headers[name] = value

#Function to get spots from LiveCQ, gets contest part of the site and parses it diretly to tight packed string
def get_livecq():
    page = get_url("http://www.livecq.eu/middencontest.asp")
    soup = BeautifulSoup(page, 'html.parser')
    livecq_lines = []
    for row in soup.findAll("tr"):
        cells = row.findAll("td")
        spot = []
        for cel in cells:
            spot.append(cel.contents)
        livecq_lines.append(spot)

    spots =[]
    spot_output = ""
    if len(livecq_lines) != 0:
        for s in livecq_lines:
            spot = []
            for y in s:
                spot.append(y[0].encode('ascii','ignore'))
            if len(spot) != 0:
                spots.append(spot)

        for spot in spots:
            spot_output += "."+spot[0].split('.')[1]+spot[1][0]+" "+spot[2]+" "

    #print spot_output
    return spot_output


#parses line from wsjt chat site to call,name,loc,date,time,message
#ouputs info in str based on mode
#and converst date+time to actual datetime object
#also compares if message includes call,name, and returns boolean based on this info

#TODO: make more checks/think what could go wrong

def strip_message(line,mode=1,notify_call=None,notify_name=None):
    notify = False
    call = "not found"
    fullcall = "not found"
    name = "not found"
    loc = "not found"
    date = "not found"
    time = "not found"
    datetime2 =  datetime.datetime.strptime("201622Oct10:29", '%Y%d%b%H:%M')
    message = ""
    try:
        time = line.split()[1]
        date = line.split()[0]
        message_tmp = line.split(' ====== {')[0].split()[2:]
        if line.find('</a>') == -1:
            call = line.split(' ====== {')[1].split()[0][0:].split('/')[0]
            fullcall = line.split(' ====== {')[1].split()[0][0:]
            name = line.split(' ====== {')[1].split()[1]
            loc = line.split(' ====== {')[1].split()[3]
        else:
            call = line.split(' ====== {')[1].split('</a>')[0].split('>')[1].split('/')[0]
            fullcall = line.split(' ====== {')[1].split('</a>')[0].split('>')[1]
            name = line.split(' ====== {')[1].split('</a>')[1].split()[0]
            loc = line.split(' ====== {')[1].split('</a>')[1].split()[2]
        for word in message_tmp:
            message = message +" "+ word
        if notify_call and notify_call.upper() in message.upper():
            notify = True
        if notify_call and notify_call.split('/')[0].upper() in message.upper():
            notify = True
        if notify_name and notify_name.upper() in message.upper():
            notify = True
        if time != "not found" and time != "not found" :
            datetime2 =  datetime.datetime.strptime("2016"+date+time, '%Y%d%b%H:%M')
    except:
        logging.error("failed to parse message: "+line)
   
    if mode == 2:
        return {'message':str(time)+" "+call + ":"+name+':: ' + message, 'notify': notify,'time': datetime2}
    if mode == 3:
        return {'message': message, 'notify': notify,'time': datetime2,'call': call,'fullcall': fullcall,'loc': loc,'name': name}
    else:
        return {'message':call + ":"+name+':: ' + message, 'notify': notify, 'time': datetime2}   

#function to parse users data for more human readeable format
def parse_users(users=users,mode=1):
    try:
        output = ""
        for user in users:
            name = users[user][4]
            call = users[user][3]
            locator = users[user][5]
            status = users[user][6]
            lastlogged = users[user][2]
            lastactive = users[user][1]
            output = output + call+" "+status+" "+lastactive.strftime("%m-%d %H:%M")+" :: "
        return output
    except Exception as e:
        logging.error("Something went wrong on parsing userdata" +str(e))
        return "Error"


#livecq loop, gets spots every 60sec, (disabled for now)

livecq = ""
def livecq_loop():

    while True:
        try:
            global livecq
            livecq = ""
            #livecq = get_livecq()
        except:
            logging.error( "failed to get livecq")
        #print livecq
        time.sleep(60)

# run livecq loop in own thread
livecqt = threading.Thread(target=livecq_loop)
livecqt.daemon = True
livecqt.start()

#Get userdata from saved cookies

for file in glob.glob("./*.txt"):
    temp =['','','','','','','','']
    logging.info(file)
    id = int(os.path.basename(file).split('.')[0])
    logging.info( str(id))
    last = datetime.datetime.fromtimestamp(os.path.getmtime(file))
    for line in open(file):
        #print line
        if "www.chris.org" in line:
            if line.split()[5] == "callsign":
                temp[0] = line.split()[6]
            elif line.split()[5] == "name":
                temp[1] = line.split()[6]
            elif line.split()[5] == "locator":
                temp[2] = line.split()[6]
            elif line.split()[5] == "state":
                temp[3] = line.split()[6]
            elif line.split()[5] == "email":
                temp[4] = line.split()[6]
    #print temp
    global users
    users[id] = (id, last, last, temp[0], temp[1], temp[2],"offline")
logging.info( str(users))

#gets initial sitedata from wsjt chat
#TODO: move this before bot init, as if this takes some time actual TelegramEMEBot message threads go nuts
#TODO: maybe some try execpt?
sitedata = ""
sitedata = get_url("http://www.chris.org/cgi-bin/review-jt65emeA")

#Send message to all users when bot starts
#TODO catch "blocked by user" (means that user has removed bot from his contacts), and somehow mark this in Users dict
for user in users:
    global bot
    try:
        logging.info("sending message, but not really")
        #bot.sendMessage(user, "I am back online (again) - you could start message feed by typing something for me",disable_notification = True,parse_mode = "Markdown")
    except Exception as e:
        logging.error( "could not send message to: " + str(user) +": "+str(e))

tempusers = str(users)

#Starts actual bot
#bot is basicly listener wich will spawn new Instanse off TelegramEMEBot when some one start private conversation whit bot.
# sets timeout for every instance now 10 000 seconds -> about 3h?
bot = telepot.DelegatorBot(TOKEN, [
    include_callback_query_chat_id(
        pave_event_space())(
            per_chat_id(types=['private']), create_open, TelegramEMEBot, timeout=20000),
])

# start bot message_loop(listener) to another thread
def startmessageloop():
    bot.message_loop(run_forever='Listening ...')

q = Queue.Queue()
tgt = threading.Thread(target=startmessageloop)
tgt.daemon = True
tgt.start()


#main thread loop, mainly purpose is to get sitedata from wsjt chat and stores it
#could also print some info to stdout from current status of program: connects, disconnect, count of users etc 
while True:
    if str(tempusers) != str(users):
        logging.info("users has changed:")
        global users
        logging.info(str(users))
        logging.info(parse_users())
        tempusers = str(users)
        if adminid != False:
            try:
                bot.sendMessage(adminid, "*Users have changed: "+ parse_users()+"*",disable_notification = True,parse_mode = "Markdown")
            except Exception as e:
                logging.error( "could not send message to: " + str(admin) +": "+str(e))
        

    try:
        sitedata = get_url("http://www.chris.org/cgi-bin/review-jt65emeA")
    except:
        logging.error( "failed to get web page")

    time.sleep(10)
    
